#!/bin/sh -ex
 
cd ../source
 
# Fake a build...
mkdir -p src/.libs
cp /usr/lib64/httpd/modules/mod_auth_gssapi.so src/.libs
 
# ... and run the tests.
./tests/magtests.py || (cat scratchdir/tests.log scratchdir/httpd/logs/error_log ; exit -1)
